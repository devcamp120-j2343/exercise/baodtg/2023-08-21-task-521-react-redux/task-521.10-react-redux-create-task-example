// import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { combineReducers, createStore } from "redux";
import taskEvent from "../components/taskEvent";

const appReducer = combineReducers({
    taskReducer: taskEvent
});

const store = createStore(
    appReducer
)
export default store