import 'bootstrap/dist/css/bootstrap.min.css'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Container, List } from 'reactstrap'

function Task() {
    const dispatch = useDispatch();

    const { tasks, taskName } = useSelector((reduxData) => reduxData.taskReducer)

    const taskNameHandler = event => {
        dispatch({
            type: "VALUE_HANDLER",
            payload: {
                taskName: event.target.value
            }
        });
    };

    const addTaskHanler = event => {
        let id = 0;

        //Hàm xử lý để lấy id cho task mới:
        if (tasks.length) {
            id = Math.max(...tasks.map(task => task.id))

            id++
        }
        if (taskName) {
            dispatch({
                type: "ADD_TASK",
                payload: {
                    task: {
                        name: taskName,
                        complete: false,
                        id
                    }
                }
            });
            dispatch({
                type: "VALUE_HANDLER",
                payload: {
                    taskName: ""
                }
            });
        }
    };
    const taskCompletedHandler = event => {
        dispatch({
            type: "TOGGLE_COMPLETED_TASK",
            payload: {
                id: event.target.id
            }
        });
    };


    return (
        <>
            <Container className='mt-5 text-center'>
                <input value={taskName} onChange={taskNameHandler} className='w-75' style={{ border: "none", borderBottom: "2px solid blue" }}></input>
                <Button onClick={addTaskHanler} color='primary' className='ms-5 '>ADD TASK</Button>
            </Container>
            <List className='mt-1 text-center'>
                {tasks.map((task, index) => {
                    return (
                        <li key={task.id}
                            style={{
                                color: task.completed ? "green" : "red",
                                listStyleType: "none"
                            }}
                            onClick={taskCompletedHandler}
                            id={task.id}
                        >
                            {index + 1}. {task.name}
                        </li>
                    )
                })}
            </List>

        </>
    )
}
export default Task